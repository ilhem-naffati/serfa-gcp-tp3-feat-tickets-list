# Utiliser une image de base avec PHP préinstallé
FROM php:apache

# Installer des dépendances nécessaires
RUN apt-get update && apt-get upgrade -y && apt-get install -y wget git unzip

# Configurer Apache
COPY ./conf/apache.conf /etc/apache2/sites-available/000-default.conf
RUN a2enmod rewrite
RUN a2enmod headers

# Télécharger et vérifier l'installateur de Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('sha384', 'composer-setup.php') === 'NOUVEAU_HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && mv composer.phar /usr/local/bin/composer

# Vérifier l'installation de Composer
RUN composer --version